package co.com.sofka.crud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class PetController {
    private PetRepository petRepository;

    @Autowired
    public PetController(PetRepository petRepository) {
        this.petRepository = petRepository;
    }

    @GetMapping(value = "api/pets")
    public Iterable<Pet> lists(){
        return this.petRepository.findAll();
    }

    @PostMapping(value = "api/pet")
    public Pet create(@RequestBody Pet pet){
        return this.petRepository.save(pet);
    }

    @DeleteMapping(value = "api/pet/{id}")
    public Iterable<Pet> deleteById(@PathVariable(value = "id", required = true) Long id) {
        petRepository.deleteById(id);
        return petRepository.findAll();
    }

    @PutMapping(value = "/api/pet/{id}")
    public Iterable<Pet> update(@RequestBody Pet pet, @PathVariable(value = "id", required = true) Long id) {
        pet.setId(id);
        petRepository.save(pet);
        return petRepository.findAll();
    }


}
