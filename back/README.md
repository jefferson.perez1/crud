## Proyecto CRUD - PET

Este demo nos permite exponer una API Rest para insertar en una base de datos en memoria, esta base de datos se borra apenas se apaga el servicio.

Expone una entidad de persistencia para guardar un PET.

## Scripts

### `gradlew build` para construir el binario
### `gradlew test` para correr los tests
### `gradlew bootRun` para poner en marcha la API

## TODO

- Implementar una capa de negocio
- Realizar los demas metodos de acceso (Edit y Delete)
- Aplicar validaciones 

## Nota importante

- El proyecto depende del fornt que corre en la dirección http://localhost:3000
- No aplicar mas dependencias de las necesarias
- Hacer el fork y aplicar los cambios dentro de su espacio de trabajo